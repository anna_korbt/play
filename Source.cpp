
//1. �������� ���� ����(%) �� �������
//2. �������� ���� �������(&) �� �������
//3. �������� ���-�� �������(&) �� 2��
//4. ������� ������ 20 ������
//5. ������� ���, ����� ����� ��������� �� ��������� ������� �� �����(�� �� � ������)
//6. ������� ���, ����� ����� �������� ��������� ���-�� ������ �� 2 �� 5
//7. ������� ���, ����� ��� ������� ������� ������� - ������� �� ������ ���������� �����(#)
//8. �������� � ���� ����� ���� '^'. �� ������ ���������� ������ ����� ������. �� ���� ������ ���������. 
//	 ���� ���� ����� ���������� - ����������. �������� �����������: ���� ����� ��������� �� ���������� � 
//	 ���� ���� �� ���� ���������� � �� ����� �� 'F' - �� ���������� ����� � ������ � 1�� �����.
//   �� ����� ������ ������ ��������� �������. 
//	 

#include <iostream>
#include <Windows.h>
#include <conio.h>
using namespace std;

const size_t col = 20u;
const size_t row = 20u;

char map[ row ][ col ] = {
	"###################",
	"#                 #",
	"#                 #",
	"#                 #",
	"#                 #",
	"#                 #",
	"# #               #",
	"#                 #",
	"#                 #",
	"#          #      #",
	"#                 #",
	"#                 #",
	"# #               #",
	"#                 #",
	"#                 #",
	"#                 #",
	"#                 #",
	"#                 #",
	"#                 #",
	"###################"
};

enum Color
{
	black,
	blue,
	green,
	cyan,
	red,
	violet,
	yellow,
	white,
	grey,
	lightBlue,
	lightGreen,
	brightCyan,
	pink,
	brightViolet,
	lightYellow,
	brightWhite
};

struct Position
{
	decltype( COORD::X ) x;
	decltype( COORD::Y ) y;
};

struct Player
{
	Position position;
	decltype(COORD::Y) HP;
};

struct Bombs
{
	int amount;
	int maxDamage;//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	int minDamage;//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
};

struct Heals
{
	int amount;
	int maxHeal;//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	int minHeal;//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
};

struct Victory
{
	Position position;
	int color;
};

struct ControlKeys
{
	char up;
	char down;
	char right;
	char left;
};

struct ObjectColors
{
	Color bomb;
	Color heal;
	Color wall;
	Color victory;
	Color player;
	Color standart;
};

struct ObjectChars
{
	char bomb;
	char heal;
	char wall;
	char victory;
	char player;
	char space;
};

//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
struct ObjectData
{
	ObjectChars chars;
	ObjectColors colors;
};

//Declarations
void generateBombs( const Bombs& bomb, const ObjectChars& ObjectChars );
void generateHeals( const Heals& heal, const ObjectChars& ObjectChars );
void generateVictory( const Victory& victory, const ObjectChars& ObjectChars );
void setCursorColor( int color );
Color getCharColor( const ObjectColors& objectColors, const ObjectChars& ObjectChars, char ch );
void printMap( const ObjectColors& objectColors, const ObjectChars& ObjectChars );
void setCursorPosition( SHORT col, SHORT row );
void showConsoleCursor( bool showFlag = true );
void setCursorPosition( Position position );
Position playerControl( Player& player, const ControlKeys& controlKeys );
void drawPlayer( const Player& player, const ObjectColors& objectColors, const ObjectChars& ObjectChars );
void smove( Player& player, const ControlKeys& controlKeys, const ObjectChars& ObjectChars );
void initialization( Bombs& bomb, Heals& heal, const Victory& victory, const ObjectColors& objectColors, const ObjectChars& ObjectChars ,Player& player);
void spawnPlayer(Player& player);

int main()
{
	Player player = { {0,0}, 20 };
	ObjectColors objectColors = { red, green, yellow, blue, brightWhite, brightWhite };
	ControlKeys controlKeys = { 'w', 's', 'd', 'a' };
	ObjectChars ObjectChars = { '%', '&', '#', '$', '@', ' ' };
	Bombs bomb = { 17 };
	Heals heal = { 9 };
	int cordX = 1 + rand() % (col - 2);
	Victory victory = { { cordX, (row - 1) }, blue };
	initialization(bomb, heal, victory, objectColors, ObjectChars,player);

	while (true)
	{
		smove(player, controlKeys, ObjectChars);
		drawPlayer(player, objectColors, ObjectChars);
		setCursorPosition(0, 21);
		cout << "                     ";
		setCursorPosition(0, 21);
		cout << "HP player : " << player.HP;
	}

	return 0;
}

void spawnPlayer(Player& player) {
	int y = 0;
	int x = 0;
	do {
		y = 1 + rand() % (row - 2);
		x = 1 + rand() % (col - 2);
	} while (map[y][x] != ' ');
	player.position.x = x;
	player.position.y = y;
}

//Implementations
void setCursorColor( int color )
{
	HANDLE handle = GetStdHandle( STD_OUTPUT_HANDLE );
	SetConsoleTextAttribute( handle, color );
}

Color getCharColor( const ObjectColors& objectColors, const ObjectChars& ObjectChars, char ch )
{
	if ( ch == ObjectChars.bomb )
		return objectColors.bomb;
	if ( ch == ObjectChars.heal )
		return objectColors.heal;
	if ( ch == ObjectChars.victory )
		return objectColors.victory;
	if ( ch == ObjectChars.wall )
		return objectColors.wall;
	if (ch == ObjectChars.player)
		return objectColors.player;

	return objectColors.standart;
}

void printMap( const ObjectColors& objectColors, const ObjectChars& ObjectChars )
{
	for ( size_t i = 0; i < row; i++ )
	{
		for ( size_t j = 0; j < col; j++ )
		{
			setCursorColor( getCharColor( objectColors, ObjectChars, map[ i ][ j ] ) );
			cout << map[ i ][ j ];
			setCursorColor( objectColors.standart );
		}
		cout << endl;
	}
}

void setCursorPosition( SHORT col, SHORT row )
{
	COORD coord = { col, row };
	SetConsoleCursorPosition( GetStdHandle( STD_OUTPUT_HANDLE ), coord );
}

void showConsoleCursor( bool showFlag )
{
	HANDLE out = GetStdHandle( STD_OUTPUT_HANDLE );

	CONSOLE_CURSOR_INFO cursorInfo;
	GetConsoleCursorInfo( out, &cursorInfo );
	cursorInfo.bVisible = showFlag;
	SetConsoleCursorInfo( out, &cursorInfo );
}

void setCursorPosition( Position position )
{
	setCursorPosition( position.x, position.y );
}

void generateBombs( const Bombs& bomb, const ObjectChars& ObjectChars )
{
	for ( int i = 0; i < bomb.amount; i++ )
	{
		map[ 1 + rand() % ( row - 3 ) ][ 1 + rand() % ( col - 3 ) ] = ObjectChars.bomb;
	}
}

void generateHeals( const Heals& heal, const ObjectChars& ObjectChars )
{
	for ( int i = 0; i < heal.amount; i++ )
	{
		map[ 1 + rand() % ( row - 3 ) ][ 1 + rand() % ( col - 3 ) ] = ObjectChars.heal;
	}
}

void generateVictory( const Victory& victory, const ObjectChars& ObjectChars )
{
	map[ victory.position.y ][ victory.position.x ] = ObjectChars.victory;
}

Position playerControl( Player& player, const ControlKeys& controlKeys )
{
	auto oldPosition = player.position;
	auto key = _getch();
	if ( key == controlKeys.right )
		player.position.x++;
	if ( key == controlKeys.left )
		player.position.x--;
	if ( key == controlKeys.up )
		player.position.y--;
	if ( key == controlKeys.down )
		player.position.y++;

	return oldPosition;
}

void drawPlayer( const Player& player, const ObjectColors& objectColors, const ObjectChars& ObjectChars )
{
	

	setCursorPosition( player.position );
	setCursorColor( objectColors.player );
	cout << ObjectChars.player;
	setCursorColor( objectColors.standart );
	
}

void smove( Player& player, const ControlKeys& controlKeys, const ObjectChars& ObjectChars )
{
	auto oldPosition = playerControl( player, controlKeys );
	setCursorPosition( oldPosition );
	cout << ' ';
	int minBombDamage = 2;
	int maxBombDamage = 5;
	int minHeal = 1;
	int maxHeal = 3;
	if ( map[ player.position.y ][ player.position.x ] == ObjectChars.bomb )
		player.HP -= minBombDamage + rand() % (maxBombDamage - minBombDamage+1);
	else if ( map[ player.position.y ][ player.position.x ] == ObjectChars.heal )
		player.HP += minHeal  + rand() % (minHeal - maxHeal+1);
	else if ( map[ player.position.y ][ player.position.x ] != ObjectChars.space )
		player.position = oldPosition;
}

void initialization( Bombs& bomb, Heals& heal, const Victory& victory, const ObjectColors& objectColors, const ObjectChars& ObjectChars ,Player& player)
{
	srand(time(nullptr));
	spawnPlayer(player);
	showConsoleCursor( false );
	generateBombs( bomb, ObjectChars );
	generateHeals( heal, ObjectChars );
	generateVictory( victory, ObjectChars );
	printMap( objectColors, ObjectChars );
}